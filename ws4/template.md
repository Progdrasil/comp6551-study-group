# Question N

> Question body

## a) Describe the semantic array. State where the solution is stored in the dynamic programming table.

## b) Describe the computational array. Do not forget the base case.

## c) Briefly argue correctness

## d) Write your algorithm in pseudocode. It should be an iterative implementation (no recursion, memoization).

## e) Justify the runtime of your dynamic programming solution.
