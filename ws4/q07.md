# Question 7

> A subsequence is palindromic if it reads the same forward as backward. For example, consider the following sequence of ASCII characters:
> * `C,O,M,P,6,6,5,1,I,S,P,I,E,C,E,O,F,C,A,K,E`
> The above sequence has many palindromic subsequences, including `6,6` and `E,C,E`.
> Another example of a palindromic subsequence is `C,P,6,6,P,C`.
> Example of a non-palindromic subsequence is `C, 6, 6, P, C`.
> Design a dynamic programming algorithm that takes a string of n ASCII characters $`x[1..n]`$
> and computes the length of the longest palindromic subsequence.
> Your algorithm should run in $`O(n2)`$ time.

See the solution from this link:
https://www.geeksforgeeks.org/longest-palindromic-subsequence-dp-12/



## a) Describe the semantic array.

## b) Describe the computational array. Do not forget the base case.

## c) Briefly argue correctness

## d) Write your algorithm in pseudocode. It should be an iterative implementation (no recursion, memoization).

## e) Justify the runtime of your dynamic programming solution.
