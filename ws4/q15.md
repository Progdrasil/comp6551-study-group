# Question 15
> Give an $`O(nt)`$ algorithm for the following task:
> - **Input:** $`A[1..n]`$ – an array of $`n`$ positive integers; $`t – a`$ positive integer.
> - **Output:** does some subset of $`A[1..n]`$ add up to exactly $`t`$?

see the solution from the links below:
https://www.geeksforgeeks.org/subset-sum-problem-dp-25/
https://www.techiedelight.com/subset-sum-problem/
